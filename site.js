"use strict";

document.addEventListener('DOMContentLoaded', setup);

function setup(){
    let button = document.createElement('input');
    button.setAttribute('type', 'submit');
    button.setAttribute('value', 'ENTER');
    document.querySelector('form').appendChild(button);
    button.addEventListener('click', build);
}

function build(e){
    e.preventDefault();
    let li = document.createElement('li');
    let ul = document.querySelector('ul');
    let numUsr = document.querySelector('#number').value;

    console.log("https://reqres.in/api/unknown/2");
    fetch("https://reqres.in/api/unknown/"+numUsr)
    .then(response => {if(!response.ok){ul.appendChild(li); li.textContent = "error";throw new Error('no data found');}
                        else {return response.json();}          
                    }).then(json => addLi(json))
                    .catch(e => {console.log('error ' + e.message);})
}

function addLi(json){
    let li = document.createElement('li');
    console.log(json);  
    let color = json.data.color;
    document.querySelector('ul').appendChild(li);
    li.textContent = `name: ${json.data.name}, year: ${json.data.year}`;
    li.style.color = color;
}